''' @file Lab0Example.py
This file acts as a simplified example for lab 0 in ME405.  This script 
simulates a coin toss instead of solving for Fibonacci '''

import random

def coin_toss():
    ''' This method flips a virtual coin and returns the result.
    @return.  The result - 1 for heads, 0 for tails.'''
    if random.random() > 0.5:
        return 1
    else:
        return 0

## String containing user input to be parsed for determining whether to flip
#  another coin or not.
user_input = input('Would you like to flip a coin? (y)es or (n)o: ')

if __name__ == '__main__':
    while(True):
        if (user_input == 'y'):
            if coin_toss() == 1:
                print('Heads')
            else:
                print('Tails')
        elif (user_input == 'n'):
            print('Thanks for playing!')
            break
        else:
            print('Invalid Input')
            
        user_input = input('Would you like to flip another coin? (y)es or (n)o: ')