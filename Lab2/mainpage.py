## @file mainpage.py
#  @author Matthew Tagupa
#  @mainpage
#
#  @section sec_intro Introduction
#  The purpose of this project was to become familiar with the motor driver 
#  and the microcontroller for our ME405 Mechatronics course.  Along with 
#  being tasked to code the enabling and disabling of the motor, we were 
#  tasked with changing the pulse width module (PWM) that whould control the 
#  speed of the motor.
#
#  @section sec_mot Motor Driver  
#  The motor driver was built connecting a X-NUCLEO-IHM04A1 motor driver 
#  platform and a NUCLEO-L476RG. The motor was connected to the A ports in the 
#  IHM04A1 along with connecting a 12V power source to port B. The program was 
#  coded in python and uses Thonny to interact between the code and the 
#  microcontroller.  Please see MotorDriver.MotorDriver which is part of the 
#  \ref MotorDriver package.
#
#  @section sec_enc Encoder 
#  The encoder was included in the motors provided with our kit. We were 
#  tasked to allow the encoder to use the internal timers in the 
#  microcontroller to count the distance rotated by the motor. Encoders A and 
#  B for the first motor will be connected to ports PB6 and PB7, respectively.
#  Encoders A and B of the second motor will be connected to ports PC7 and PC6,
#  respectively. Please see Encoder.Encoder which is part of the \ref Encoder 
#  package.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 6, 2020
#