## @file Encoder.py
#  This is the encoder that is build into the motors provided in our ME 405 
#  kits. When installed correct, the encoder allows the microcontroller to 
#  count encoder ticks since the controller cannot understand distances, 
#  angles, nor direction. This code chen called will help the user designate 
#  distances and angles by giving encoder ticks. It is up to the user to find
#  the approximation of those distances and angles.
#
#  This class was built to function with the NUCLEO and motor driver. The user 
#  will be required to designate the ports being used for the encoder. The user 
#  will also input the timer number, Encoder ports from the motors, the 
#  prescaler, and the period (number of counts), respectively. The lines of 
#  code given in the main part of this code is an example of what the user 
#  needs to input when implementing two different motors.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020
#
#  @package Encoder
#  This is the encoder that is build into the motors provided in our ME 405 
#  kits. When installed correct, the encoder allows the microcontroller to 
#  count encoder ticks since the controller cannot understand distances, 
#  angles, nor direction. This code chen called will help the user designate 
#  distances and angles by giving encoder ticks. It is up to the user to find
#  the approximation of those distances and angles.
#
#  This class was built to function with the NUCLEO and motor driver. The user 
#  will be required to designate the ports being used for the encoder. The user 
#  will also input the timer number, Encoder ports from the motors, the 
#  prescaler, and the period (number of counts), respectively. The lines of 
#  code given in the main part of this code is an example of what the user 
#  needs to input when implementing two different motors.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020

import pyb

## An encoder driver object
#
#  This class implements the encoder for the ME 405 board.
#
#  @author Matthew Tagupa
#  @copyright License Info
#  @date May 21, 2020
class Encoder:

    ## Constructor for encoder driver
    #
    #  This constructor is set to initialize the timer that will be used to 
    #  count the ticks that are output from the encoder into the motor driver.
    #
    #  @param TimerNo The timer that the encoder will use
    #  @param EncoderA_pin The pin designated for encoder channel A
    #  @param EncoderB_pin The pin designated for encoder channel B
    #  @param prescaler The initial value used to set the encoder
    #  @param period The limit where the counting in the encoder will stop
    def __init__(self, TimerNo, EncoderA_pin, EncoderB_pin, prescaler, period): 
        
        self.EncoderA_pin = pyb.Pin(EncoderA_pin, pyb.Pin.IN)
        self.EncoderB_pin = pyb.Pin(EncoderB_pin, pyb.Pin.IN)
        
        self.prescaler = prescaler
        self.period = period
        
        # This sets up the timer as a counter from the information provided by
        # the encoders.
        self.timer = pyb.Timer(TimerNo, prescaler = self.prescaler, period = self.period)
        
        self.timer.channel(1, self.timer.ENC_A, pin = self.EncoderA_pin)
        self.timer.channel(2, self.timer.ENC_B, pin = self.EncoderB_pin)
        
        # This line is needed for the initialization of the update method.
        self.position = 0
        self.last_position = 0

    
    ## Updated Position
    #
    #  When called, this will update the recorded position of the encoder to 
    #  the current position.
    def update(self):
        # ...and the current update location
        self.current_position = self.timer.counter()
        
        # This is the original calculation to find the differnece in the old 
        # update location from the new update location.
        self.delta = self.current_position - self.last_position
        
        # In case we have overflow or underflow, we will need to check the 
        # following...
        #
        # In the scenario of an underflow,
        if (self.delta > self.period/2):
            self.delta = self.delta - self.period
            
            # We must continue counting the position
            self.position += self.delta
        
        # In the scenario of an overflow,
        elif (self.delta < -((self.period/2) + 1)):
            self.delta = self.delta + self.period
            
            # We must continue counting the position
            self.position += self.delta
            
        # If there was no underflow or overflow,
        else:
            self.position += self.delta
        
        # We are recording the last update location...
        self.last_position = self.current_position

    ## Gets the encoder's position
    #
    #  When called upon, this method gets the encoder's current position and 
    #  displays that position.
    def get_position(self):
        
        return self.position
    
    ## Sets the encoder's position
    #
    #  When called upon, this method will reset the encoder to the specified 
    #  position. The position will be an argument assigned inside of the 
    #  set_position method.
    #
    #  @param new_position The position you want the encoder to read starting from
    #  when this command is activated
    def set_position(self, new_position):
        self.new_position = new_position
        self.last_position = self.new_position
        self.position = self.new_position

    ## Finds the change between 2 update methods
    #
    #  When called upon, this method returns the difference in recorded 
    #  position between the two most recent calls to update().
    def get_delta(self):
        
        return self.delta
    
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.
    
    # Designate encoder timer number (4 or 8)
    Timer1 = 4
    Timer2 = 8
    
    # Designate prescaler
    prescaler = 0
    
    # Designate period
    period = 65535
    
    # Designate pins for Encoder A and Encoder B on the motor
    EncoderA1 = pyb.Pin.cpu.B6
    EncoderB1 = pyb.Pin.cpu.B7
    
    EncoderA2 = pyb.Pin.cpu.C6
    EncoderB2 = pyb.Pin.cpu.C7
    
    # Enable the Encoder
    enco1 = Encoder(Timer1, EncoderA1, EncoderB1, prescaler, period)
    enco2 = Encoder(Timer2, EncoderA2, EncoderB2, prescaler, period)
