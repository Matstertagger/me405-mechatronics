## @file PropControl.py
#  This functions as a proportional controller for our motor driver included
#  with our ME 405 kits. This controller will be used to regulate the response 
#  of the motor. It will be the users responsibility to designate the gain 
#  to receive the response that they want. 
#
#  This class was built to function with the NUCLEO, motor driver, and 
#  encoder. The user will designate the location (setpoint) they want the 
#  motor to stop by designating a number of encoder ticks to stop and how fast 
#  they want the system to reach that location. WARNING: The response of the 
#  motor is gradual and should not be expected to reach the designated point 
#  instantly. We want to manage this by creating a closed-loop controller. For 
#  now, this class will be given a Kp value to to keep constant throughout the 
#  process of running the main code (main.py). Since this class also acts as 
#  the closed-loop part of the response, it will add the setpoint to the 
#  encoder ticks measured by the encoder and will multiply that result by the 
#  gain, Kp.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020
#
#  @package CLPropControl
#  This functions as a proportional controller for our motor driver included
#  with our ME 405 kits. This controller will be used to regulate the response 
#  of the motor. It will be the users responsibility to designate the gain 
#  to receive the response that they want.
#
#  This class was built to function with the NUCLEO, motor driver, and 
#  encoder. The user will designate the location (setpoint) they want the 
#  motor to stop by designating a number of encoder ticks to stop and how fast 
#  they want the system to reach that location. WARNING: The response of the 
#  motor is gradual and should not be expected to reach the designated point 
#  instantly. We want to manage this by creating a closed-loop controller. For 
#  now, this class will be given a Kp value to to keep constant throughout the 
#  process of running the main code (main.py). Since this class also acts as 
#  the closed-loop part of the response, it will add the setpoint to the 
#  encoder ticks measured by the encoder and will multiply that result by the 
#  gain, Kp.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020

import pyb

## A proportional controller.
#
#  This class implements a proportional controller for our ME 405 board.
#
#  @author Matthew Tagupa
#  @copyright License Info
#  @date May 21, 2020
class CLPropControl:

    ## Constructor for Proportional Controller
    #
    #  This constructor is set to initialize the timer that will be used to 
    #  count the ticks that are output from the encoder into the motor driver.
    #  
    #  @param Kp The proportional gain
    def __init__(self, Kp): 
        
        #  This is the proportional gain
        self.Kp = Kp

    ## Updated Position
    #
    #  When called, this will update the recorded position of the encoder to 
    #  the current position.
    #
    #  @param setpoint The Referenced value that the controller will compare 
    #  to the output value
    #  @param Theta_Measured The measured output value
    def update(self, setpoint, Theta_Measured):
        
        #  We put setpoint here in case the setpoint moves
        self.setpoint = setpoint
        
        #  Get the measured theta
        self.Theta_Measured = Theta_Measured
        
        #  Calculate the ERROR
        self.Theta_ERROR = self.setpoint - self.Theta_Measured
        
        #  Multiply by Kp
        self.actuation_value = self.Theta_ERROR * self.Kp
        
        #print('Actuation Value = {:}'.format(self.actuation_value))
        
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.
    
    ## The setpoint
    T_ref = 1000
    
    ## The Proportional Gain
    Prop_Gain = 0.1
    
    ## Creating the closed-loop variable for the controller
    Control = CLPropControl(Prop_Gain)