## @file IMU.py
#  The Inertial Measurement Unit (IMU) is a combination of the BNO055 and the 
#  BMP280 allowing it to read acceleration, gyroscopic motion, and magnetic 
#  field strength. All can be read from the microcontroller after plugging the 
#  IMU into the NUCLEO (5V, GND, SDA, and SCL) in the correct ports. The user 
#  needs to plug the SDA and SCL to the same coordinated bus. This will 
#  require the user to look for information an all of the ports on their 
#  microcontroller and plug in these cables from the IMU to those ports.
#
#  The user will be responsible for plugging the Internal Measurement Unit 
#  (IMU) into the correct ports in the NUCLEO and designating the ports used 
#  for the bus. In the example shown underneath the class, I connected the SCL 
#  (blue cable) to PB8 on the NUCLEO and SDA (green cable) to PB8 on the 
#  NUCLEO. this coordination can be found in the link that follows. Make sure 
#  the SDA and SCL have matching bus numbers. After correctly connecting the 
#  IMU to the NUCLEO board and keeping track of the bus number, the user will 
#  designate the bus number along with marking the NUCLEO as the master 
#  ('I2C.MASTER'). Those will be passed to the class which will allow the 
#  NUCLEO to read and write information from the IMU (slave). 
#
#  NULCEO L476RG Pin Assignment: https://os.mbed.com/platforms/ST-Nucleo-L476RG/#nucleo-pinout
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date June 2, 2020
#
#  @package IMU
#  The Inertial Measurement Unit (IMU) is a combination of the BNO055 and the 
#  BMP280 allowing it to read acceleration, gyroscopic motion, and magnetic 
#  field strength. All can be read from the microcontroller after plugging the 
#  IMU into the NUCLEO (5V, GND, SDA, and SCL) in the correct ports. The user 
#  needs to plug the SDA and SCL to the same coordinated bus. This will 
#  require the user to look for information an all of the ports on their 
#  microcontroller and plug in these cables from the IMU to those ports.
#
#  The user will be responsible for plugging the Internal Measurement Unit 
#  (IMU) into the correct ports in the NUCLEO and designating the ports used 
#  for the bus. In the example shown underneath the class, I connected the SCL 
#  (blue cable) to PB8 on the NUCLEO and SDA (green cable) to PB8 on the 
#  NUCLEO. this coordination can be found in the link that follows. Make sure 
#  the SDA and SCL have matching bus numbers. After correctly connecting the 
#  IMU to the NUCLEO board and keeping track of the bus number, the user will 
#  designate the bus number along with marking the NUCLEO as the master 
#  ('I2C.MASTER'). Those will be passed to the class which will allow the 
#  NUCLEO to read and write information from the IMU (slave). 
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date June 2, 2020

import utime
import pyb
from pyb import I2C
import ustruct
from micropython import const

## A Inertial Measurement Unit (IMU) object
#
#  This class reads and writes data from the IMU to our ME 405 board.
#
#  @author Matthew Tagupa
#  @copyright License Info
#  @date June 2, 2020

## Constants
#
#  NDOF mode activates all 3 sensors
NDOF = const(0x0C)
#  Power mode- address within the slave
PWR = const(0x3E)
#  Pauses the system
suspend = const(2)
#  Slave address
BNO055 = const(0x28)
#  OPR_MODE- address within the slave
OPR = const(0x3D)
#  Lower byte of X axis Acceleration data
ACC_DATA = const(0x08)
#  Lower byte of X axis Gyroscope data
GYR_DATA = const(0x14)
#  Lower byte of X axis Magnetometer data
MAG_DATA = const(0x0E)
#  Lower byte of heading data
EUL_DATA = const(0x1A)
#  Calibration status register
Calib_Stat = const(0x35)
#  Used to convert acceleration data to m/s^2 (1 m/s^2 = 100 LSB AND 1 mg = 1 
#  LSB)
Accel_Convertion = const(100)
#  Used to convert gyroscopic data to degrees/second (1 Deg/sec = 16 LSB and 1 
#  Rad/sec = 900 LSB)
GYRO_Convertion = const(16)
#  Used to convert euler angle data to degrees (1 Deg = 16 LSB and 1 Rad = 900 
#  LSB)
EUL_Convertion = const(16)

class IMU:
    
    ## Constructor for the IMU
    #
    #  Creates a motor driver by initializing GPIO pins and turning the motor 
    #  off for safety.
    #
    #  @param i2c The designation of the master to the IMU and the bus the 
    #  slave is connected to
    def __init__(self, i2c):
        
        self.i2c = i2c
        
    ## Enable the IMU
    #
    #  This method enables the IMU. The IMU will be enabled upon start-up.
    def  enable (self):
        # Place the system in normal mode. All sensors are always switched ON 
        #  in this mode.
        i2c.mem_write(PWR, BNO055, 0)
        
    ## Disable the IMU
    #
    #  This method disables the IMU.
    def disable (self):
        #  Pause the system and places all sensors and microcontroller into 
        #  sleep mode
        i2c.mem_write(PWR, BNO055, 2)        
    
    ## Set the mode of the IMU
    #
    #  This method sets the mode of the IMU. Our code will automatically put 
    #  it in the nine degrees of freedom (NDOF) mode.
    def set_mode(self):
        #Sets the operation mode to NDOF (This step fully enables the imu)
        i2c.mem_write(NDOF, BNO055, OPR)
        
    ## Get the mode of the IMU
    #
    #  This method displays the current mode of the IMU.
    def get_mode(self):
        data = i2c.mem_read(1, BNO055, OPR)
        
        #  Extract the first value from the tuple
        self.mode = ustruct.unpack('<b', data)[0]
        
        #  Creates a library with some of the modes available
        MODES = dict([('NDOF MODE', 12), ('CONFIG MODE', 0)])  
        print('MODES =', MODES)
        if (self.mode == 12):
            print('The BNO055 is in NDOF mode')
        else:
            pass
    
    ## Get the acceleration data off the IMU
    #
    #  This method displays the acceleration the IMU is experiencing. The 
    #  values are returned as a tuple in their respective Cartesian axis 
    #  directions (x,y,z) in m/s^2.
    def get_accel(self):
        #  Read 6 bytes representing the LSB and MSB of the acceleration 
        #  sensor
        accel_data = self.i2c.mem_read(8, BNO055, ACC_DATA)
        #  Unpack the data into a 3-tuple [units are in mg]
        x, y, z = ustruct.unpack('<hhh', accel_data)    
        
        #Divide by the convertion factor to get the data in m/s^2
        x = x/Accel_Convertion
        y = y/Accel_Convertion
        z = z/Accel_Convertion
        return (x,y,z)
    
    ## Get the angular velocity data off the IMU
    #
    #  This method displays the angular velocity the IMU is experiencing. The 
    #  values are returned as a tuple in their respective Cartesian axis 
    #  directions (x,y,z) in deg/sec.
    def get_gyro(self):
        gyro_data = self.i2c.mem_read(8, BNO055, GYR_DATA)
        x, y, z = ustruct.unpack('<hhh', gyro_data)
        
        #Divide by the convertion factor to get the data in deg/sec
        x = int(x/GYRO_Convertion)
        y = int(y/GYRO_Convertion)
        z = int(z/GYRO_Convertion)
        return (x,y,z)
    
    ## Get the Euler angle data off the IMU
    #
    #  This method displays the Euler angles the IMU is showing. From testing, 
    #  it seems that the IMU reads 0 degrees for the rotation about the z-axis 
    #  when the x-axis is pointed east. The values are returned as a tuple in 
    #  their respective Cartesian axis directions (x,y,z) in deg/sec.
    def get_eul(self):
        euler_data = self.i2c.mem_read(6, BNO055, EUL_DATA)
        
        x, y, z = ustruct.unpack('<hhh', euler_data)
        
        #Divide by the convertion factor to get the data in degrees
        x = int(x/EUL_Convertion)
        y = int(y/EUL_Convertion)
        z = int(z/EUL_Convertion)
        return (x,y,z)
    
    ## Get the calibration status of the IMU
    #
    #  This method checks and returns the calibration status of the IMU. The 
    #  IMU will read the calibration status of each sensor and return a value 
    #  between 0 and 3. A value of 0 means that the sensor is not calibrated 
    #  and 3 means that it is fully calibrated. Calibrating is not difficult 
    #  and it does not take long, but it is recommended that when adding this 
    #  class to the main code the coder should include some indication that 
    #  the sensors are fully calibrated. To do this, simply ask the user to 
    #  first hold the sensor flat on a flat surface for 3 seconds, followed by
    #  rolling, pitching, and yaw the device and hold them in those angles for 
    #  about 3 seconds each, and finally holding the devise by the connection 
    #  cables and creating figure 8's with the device until each sensor 
    #  returns a value of 3. 
    def get_calibration(self):

        #  Retrieve the calibration register located at 0x35
        calib_data = self.i2c.mem_read(NDOF, BNO055, Calib_Stat)
        
        #  Shift the data from the calibration register to the right as 
        #  needed, then mask the result in order to only display the bits 
        #  desired.
        
        #  Magnetometer calibration status
        self.mag_cal = (calib_data[0] >> 0) & 0b11
        
        #  Acceleration calibration status
        self.acc_cal = (calib_data[0] >> 2) & 0b11
        
        #  This will shift the register bits 4 and 5 to the right four times 
        #  in order to get them into bits 0 and 1 (right alinged). The result 
        #  is then masked with 00000011 in order to clear all bits except the 
        #  first two.
        
        #  Gyroscope calibration status
        self.gyr_cal = (calib_data[0] >> 4) & 0b11
        
        
        print('The magnetometer calibration status is: ', self.mag_cal,'\n' 
              'The acceleration calibration status is: ', self.acc_cal,
              '\n' 'The gyroscope calibration status is: ', self.gyr_cal)

if __name__ =='__main__':
    #  Any code within the if __name__ == '__main__' block will only run when 
    #  the script is executed as a standalone program. If the script is 
    #  imported as a module the code block will not run.
    
    #  Designate the bus that the IMU is connected to on the NUCLEO
    bus = 1
    
    #  Create the I2C on bus 1 and init as a master 
    i2c = I2C(bus, I2C.MASTER)
    
    #  Creating the IMU object and passing the I2C bus in to the constructor
    imu = IMU(i2c)
    
    #  Set the mode
    imu.set_mode()
    
    #  Check the mode
    imu.get_mode()

    for i in range (0, 150):
        #imu.get_calibration()
        print('{}'.format(imu.get_eul()))
        #print('{}'.format(imu.get_gyro()))
        utime.sleep_ms(1000)
