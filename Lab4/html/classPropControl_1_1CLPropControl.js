var classPropControl_1_1CLPropControl =
[
    [ "__init__", "classPropControl_1_1CLPropControl.html#a22c69cbbf80621704cee4355638b9251", null ],
    [ "update", "classPropControl_1_1CLPropControl.html#adfc565f15d496d00ad4f86b7fa04d710", null ],
    [ "actuation_value", "classPropControl_1_1CLPropControl.html#ac14c756f787f2126a2b98a05b73f750e", null ],
    [ "Kp", "classPropControl_1_1CLPropControl.html#a51a708a573d89c8f73cf662383586f72", null ],
    [ "setpoint", "classPropControl_1_1CLPropControl.html#a5380900a48cb8b4c21bc8e4c3e4d8fe6", null ],
    [ "Theta_ERROR", "classPropControl_1_1CLPropControl.html#a32614cb0c6e4ddea34ae2fc42fcfb6dc", null ],
    [ "Theta_Measured", "classPropControl_1_1CLPropControl.html#a5268cfd5f60e373e12bea21d9af6a278", null ]
];