## @file main.py
#  This is an available example of how to input the functions of the
#  motor driver into the REPL.
#
#  This serves only as an example. MotorDriver.py should be able to
#  function as an example as well with a duty cycle set to 25%.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date April 29, 2020
#
#

import MotorDriver

# Create the pin objects used for interfacing with the motor driver
pin_EN = 1;
pin_IN1 = 0;
pin_IN2 = 1;

# Create the timer object used for PWM generation
timer = 500;

# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, timer)

# Enable the motor driver
moe.enable()

# Set the duty cycle to 10 percent
moe.set_duty(10)