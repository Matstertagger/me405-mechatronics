## @file main.py
#  This is the main code that will combine the motor driver, encoder, and the 
#  proportional gain classes into a functioning step response for our motors.
#  It will be the users responsibility to read the other codes to see what the 
#  input arguments are for each class and their methods.
#
#  Since this is the main code that will be running the step response, it 
#  will call the motor driver, encoder, and the poroportional controller to 
#  calculate and output the position and its associated time of measurement.
#  As mentioned above it is the users responsibility to designate the pins,
#  and the constants they would like to use for their motor, encoder, and 
#  proportional controller. It is highly recommended to view the classes
#  MotorDriver, Encoder, and CLPropControl to understand what is occuring in 
#  each of the methods in those classes. This code already runs the template 
#  that each contains in the main portion of their respective code files, 
#  however, those portions of those files will not be run when calling on the 
#  class. The classes will only recognize this file for the inputs.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020
#
#  @package main
#  This is the main code that will combine the motor driver, encoder, and the 
#  proportional gain classes into a functioning step response for our motors.
#  It will be the users responsibility to read the other codes to see what the 
#  input arguments are for each class and their methods.
#
#  Since this is the main code that will be running the step response, it 
#  will call the motor driver, encoder, and the poroportional controller to 
#  calculate and output the position and its associated time of measurement.
#  As mentioned above it is the users responsibility to designate the pins,
#  and the constants they would like to use for their motor, encoder, and 
#  proportional controller. It is highly recommended to view the classes
#  MotorDriver, Encoder, and CLPropControl to understand what is occuring in 
#  each of the methods in those classes. This code already runs the template 
#  that each contains in the main portion of their respective code files, 
#  however, those portions of those files will not be run when calling on the 
#  class. The classes will only recognize this file for the inputs.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020

import pyb
import utime
import array

## The main code.
#
#  This is the main code that will use the combinationof the encoder, motor 
#  driver, and the proportional controller to create and test the step 
#  response of the motor
#
#  @author Matthew Tagupa
#  @copyright License Info
#  @date May 21, 2020

from MotorDriver import MotorDriver

from Encoder import Encoder

from PropControl import CLPropControl
    
if __name__ == '__main__':
    
## Initialization-------------------------------------------------------------

    ## Ask for the user to input whether they want to run this code 
    user_input = input('Would you like to find response curves?'
                       '(y)es or (n)o: ')
    
    #  This loop is for the code to run through the calculations for the step 
    #  response then ask for another Kp value, repreatedly 
    while (user_input == 'y'):
        
        #  This will ask the user to input a value for Kp. The value for Kp 
        #  must be a positive number. 
        while (user_input == 'y'):
            try:
                Kp = float(input('Please enter a positive value for Kp: '))
            except ValueError:
                print('\nInvalid Input. Please enter a positive number.')
                continue
                # This continue function will return the code to the top
                # top of the current while loop
            # This next if function is to make sure that the input is a
            # positive number
            if (Kp < 0):
                print('\nInvalid Input. Please enter a positive number.')
                continue
            else:
                break
                # This break function exits this current while loop
        
        #  Motor Driver
        #
        #  Create the pin objects used for interfacing with the motor driver
        pin_EN_A  = pyb.Pin.cpu.A10
        pin_IN1_A = pyb.Pin.cpu.B4
        pin_IN2_A = pyb.Pin.cpu.B5

        ## Create the timer object used for PWM generation
        timer_number_A = 3
    
        ## Designate the frequency
        freq = 20000

        ## Create a motor object passing in the pins and timer
        moeA = MotorDriver(pin_EN_A, pin_IN1_A, pin_IN2_A, timer_number_A, freq)

        #  Enable the motor driver
        moeA.enable()

        #  Set the duty cycle to 0 percent to start. It will be set by the 
        #  controller
        moeA.set_duty(0)
    
    
        ## Encoder
        #
        #  Designate encoder timer number (4 or 8)
        Timer1 = 4
        Timer2 = 8
    
        #  Designate prescaler
        prescaler = 0
    
        #  Designate period
        period = 65535
    
        #  Designate pins for Encoder A and Encoder B on the motor
        EncoderA1 = pyb.Pin.cpu.B6
        EncoderB1 = pyb.Pin.cpu.B7
    
        #  Enable the Encoder
        enco1 = Encoder(Timer1, EncoderA1, EncoderB1, prescaler, period)
    
        #  Need to initialize this current delta for the actuation value
        #  calculations
        delta_sum = 0
    
    
        ## PropControl
        #
        #  We need to pass in the Kp value to initialize the proportional 
        #  controller. This is also where we are going to define the setpoint.
        PropControl = CLPropControl(Kp)
        Theta_ref = 1000
    
        ## Create array for output
        #
        #  These array setups will be used for the step response for the 
        #  different proportional gains. Our graph to show the response  
        #  behavior will be position measured in encoder ticks vs time 
        #  measured in milliseconds
        Position_array = array.array('i', [])
        Time_array = array.array('i', [])
    
## Main Program-------------------------------------------------------------------------------------------

        #  This part of the code is the controller in a closed-loop response.
        #  We are going to limit the number of loops to 200 iterations. We are
        #  doing this because I had first tried it with a while loop and the 
        #  issue that was occuring was due to the accuracy of the encoder and 
        #  the motor simultaneously working together. If the encoder picks up 
        #  an overshoot from the reference value, the microcontroller will 
        #  quickly try to correct this overshoot by reversing the direction of 
        #  the motor. Since the motor taks some time to reverse directions, 
        #  when the output position tries to return to the setpoint value, it
        #  will overshoot again and repeat this process until the user tells 
        #  it to stop or the program eventually crashes. 
        for i in range (0, 200):
            
            #  We first need to call on the encoder update method to get the
            #  current position and the difference in that position from the
            #  previous position of the motor.
            enco1.update() 
            delta_update = enco1.delta
    
            #  We are now going to add the accumulated positions together to 
            #  know how much closer the motor is to the desired setpoint
            delta_sum = delta_sum + delta_update
            
            #  This next line of code is not necessary, I included it with the 
            #  intent to help me and the user understand what is being passed 
            #  back to the closed-loop proportional controller.
            Theta_Measured = delta_sum
        
            #  Call the update method from the proportional controller so that
            #  the actuation value can be given to the MotorDriver class to 
            #  change the value of the PWM
            PropControl.update(Theta_ref, Theta_Measured)
            actuation = PropControl.actuation_value
        
            #  Update the MotorDriver duty cycle (PWM)
            moeA.set_duty(actuation)

            #  This is a delay that will allow the recording of the values to 
            #  be more gradual not performing this may cause the program to 
            #  crash due to the large amount of iterations it must perform. 
            utime.sleep_ms(10)
            
            #  These will take those values and place them into arrays that 
            #  will be printed after the loop has finished. It is worthy 
            #  information to know that the utime.ticks_ms records a time 
            #  every millisecond and it never resets (in a timely 
            #  manner). 
            Position_array.append(enco1.current_position)
            Time_array.append(utime.ticks_ms())
    
        else:
            #  When the for loop is done, this will disable the motor and 
            #  proceed to the next loop that will print the arrays
            moeA.disable()
        
        for j in range (0, 200):
            #  When printing the arrays, we want to be sure that there is a 
            #  comma between the values. This makes copying and pasting the 
            #  data into a spreadsheet simple and can help with formatting.
            #  Also keep in mind about what I said with the internall clock 
            #  never going back to zero. In the spreadsheet it is a good idea 
            #  to subtract all of the time values by the first recorded time 
            #  of that run of the program. This way, all of the responses 
            #  start at time 0 when graphing the response.
            print(Time_array[j],',', Position_array[j])

    else:
        #  This is just in case the user doesn't want to initially enter a 
        #  value for the proportional gain, Kp
        print('Ending simulation')