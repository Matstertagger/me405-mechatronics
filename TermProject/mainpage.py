## @file mainpage.py
#  @author Matthew Tagupa
#  @mainpage
#
#  @section sec_intro Introduction
#  The purpose of this project was to become familiar with the motor driver 
#  and the microcontroller for our ME405 Mechatronics course.  Along with 
#  being tasked to code the enabling and disabling of the motor, we were 
#  tasked with changing the pulse width module (PWM) that whould control the 
#  speed of the motor.
#
#  @section sec_mot Motor Driver  
#  The motor driver was built connecting a X-NUCLEO-IHM04A1 motor driver 
#  platform and a NUCLEO-L476RG. The motor was connected to the A ports in the 
#  IHM04A1 along with connecting a 12V power source to port B. The program was 
#  coded in python and uses Thonny to interact between the code and the 
#  microcontroller.  Please see MotorDriver.MotorDriver which is part of the 
#  \ref MotorDriver package.
#
#  Repository Link to MotorDriver.py: https://bitbucket.org/Matstertagger/me405-mechatronics/src/master/Lab3/MotorDriver.py
#
#  @section sec_enc Encoder 
#  The encoder was included in the motors provided with our kit. We were 
#  tasked to allow the encoder to use the internal timers in the 
#  microcontroller to count the distance rotated by the motor. Encoders A and 
#  B for the first motor will be connected to ports PB6 and PB7, respectively.
#  Encoders A and B of the second motor will be connected to ports PC7 and PC6,
#  respectively. Please see Encoder.Encoder which is part of the \ref Encoder 
#  package.
#
#  Repository Link to Encoder.py: https://bitbucket.org/Matstertagger/me405-mechatronics/src/master/Lab3/Encoder.py
#
#  @section sec_prop Closed-Loop Proportional Controller
#  The Closed-Loop Proportional Controller is a code that I included with this
#  set of instructions. The proportional controller will be given a 
#  proportional gain, a setpoint, and the measured angle of the motor by the 
#  encoder. This was added to have a more accurate response of the motor from
#  0 to the designated setpoint. Please see PropControl.CLPropControl which is 
#  part of the \ref CLPropControl package.
#
#  Repository Link to PropControl.py: https://bitbucket.org/Matstertagger/me405-mechatronics/src/master/Lab3/PropControl.py
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020
#
#  @page page_prop Step Responses for Different Proportional Gains
#
#  @image html Proportional_Gain_Responses.png
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020
#
#  @page page_imu Inertial Measurement Unit (IMU)
#
#  The Inertial Measurement Unit (IMU) is a combination of the BNO055 and the 
#  BMP280 allowing it to read acceleration, gyroscopic motion, and magnetic 
#  field strength. All can be read from the microcontroller after plugging the 
#  IMU into the NUCLEO (5V, GND, SDA, and SCL) in the correct ports. The user 
#  needs to plug the SDA and SCL to the same coordinated bus. This will 
#  require the user to look for information an all of the ports on their 
#  microcontroller and plug in these cables from the IMU to those ports. 
#  Please see IMU.IMU which is 
#  part of the \ref IMU package.
#
#  Youtube Video Link: https://youtu.be/V5fQkMNBrMQ
#
#  Repository Link to IMU.py: https://bitbucket.org/Matstertagger/me405-mechatronics/src/master/Lab4/IMU.py
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date June 2, 2020
#
#  @page page_Proposal ME 405 Term Project Proposal: "Do Nothing Box"
#
#  @section sec_title ME 405 Term Project Proposal: The Do Nothing Box
#  Team Members: Jose Chavez, Trevor Blythe, and Matthew Tagupa
#  Link: https://matstertagger.bitbucket.io
# 
#  @section sec_problem Problem Statement
#  Have you ever found yourself bored and doing nothing? We built a box that 
#  is just as bored and also does absolutely nothing to entertain itself. 
#  However, with great teamwork, both you and the ''Do Nothing Box'' can work 
#  together to entertain each other.
# 
#  Our ''Do Nothing Box'' has state of the art ME 405 features including a 
#  Nucleo board and motor driver to control two motors and a switch that will 
#  all be used for fun. The user will be tempted to turn on the switch 
#  activating the ''Do Nothing Box''. At this point, the first motor in the box 
#  will open the box from the inside while the second motor controlling a 
#  mechanical arm in the box controlled by the motor will reach out of the box 
#  to turn the switch off then retract back into the box. The encoders built 
#  into the motors will be in charge of making sure that the correct amount of 
#  stress is placed on the switch and the lid of the box to function 
#  appropriately without damaging the motor or any of the parts inside of the 
#  motor. 
# 
#  @section sec_materials Bill of Materials: 
#  - A box, ambitiously wooden but cardboard will do
#  - Gears for the motor (need gears that can be put onto a shaft with 
#  dimensions: diameter = 3 mm) (some affordable options found on Amazon)
#  - Gears for the lever arms (Gear set on Amazon
#  - Shafts for the lever arm (Find gear set on Amazon)
#  - Wood for the lever arm (Home improvement store)
#  - Toggle Switch (Amazon)
# 
#  @section sec_plan Assembly Plan:
#  Some of us have the tools for this project, for example the lever arm needs 
#  to be made possibly with wood which we can all get at a home improvement 
#  store, but not all of us have woodshop tools. We are devising a plan to 
#  make it easier for all members to be able to build. This includes, but is 
#  not limited to, building the arm with junk around the house like soda cans.
# 
#  @section sec_safety Safety Assessment:
#  The project is mostly safe, with most of the danger coming from the use of 
#  hand tools during the construction and assembly. To stay safe, basic hand 
#  tool safety will be observed. There is a small amount of danger that may 
#  arise from sticking fingers in pinch points while the motor turns, but this 
#  will not be hard to avoid.
# 
#  @section sec_timeline General Timeline:
#  We will start by ordering parts, so that we don’t have to wait for them to 
#  arrive. Testing the physical design will also start early, so that any 
#  potential problems can be ironed out while we still have time to purchase 
#  new parts. As this is taking place, we will also work on the code that 
#  controls the most basic functions. From here, we can iron out details, and 
#  then slowly add new features as time permits. We are planning on meeting 
#  every other day for an hour or two and for about 4 to 5 hours over the 
#  weekend to make sure that the code gets done and each member’s ''Do Nothing 
#  Box'' works perfect. We plan on having the code and a final product to test 
#  by the Tuesday of finals week (6/9). 
#
#  @author Matthew Tagupa, Trevor Blythe, and Jose Chavez
#
#  @copyright License Info
#
#  @date May 22, 2020
#
#  @page page_TermProject ME 405 Term Project: "Do Nothing Box"
#
#  @section sec_abstract Abstract
#  Starting May 22, 2020, Jose Chavez, Trevor Blythe and I started the "Do 
#  Nothing Box" project. We knew going into this project that the coding would 
#  be relatively easy since it was an expansion of all of the other labs we 
#  had previously and successfully created. However, the hardware would take 
#  much longer and would be more precise for there to be no damage to the 
#  functioning components. Since the "Do Nothing Box" needed some tight 
#  tolerances since I was working with smaller components. The lack of tools 
#  at my disposal and wood shop experience, my project does not have a nice 
#  finish on it and a day before the duue date, one of the motors 
#  malfunctioned and is now rendered useless.
#
#  This is a link to the video of what I was able to create with one motor:
#  https://www.youtube.com/watch?v=OMJguHxjEUg&feature=youtu.be
#
#  @section sec_coding Coding
#  If you are familiar with the other codes from this ME 405 lab, 
#  understanding this code is not much different. This project was essentially
#  Lab 3 with a switch being used as part of the input. The only thing needed 
#  to include the switch was to create an input pin for it. The code should be 
#  able to multitask between the motor that will open the door and the motor 
#  that will cause the lever arm to turn the switch OFF. Please see 
#  ToggleSwitch.ToggleSwitch which is part of the \ref ToggleSwitch package.
#
#  @section sec_shell The Box
#  The box design itself took the longest amount of time to figure out. We 
#  knew we needed our box to be big enough to ouse the microcontroller, the 
#  motors and any components that needed to be added to the motors (gears, 
#  lever arm, hatch links, etc.). Jose and I were very open minded when it 
#  came to the box design making it difficult to make decisions towards the 
#  best design. The documents that follow show some of the design options that 
#  I imagined for building the box. Our final design is presented on the 
#  second image. 
#
#  Design Files:
#  @image html Design1.png
#  @image html Design2.png
#
#  I built my box using 1/4" plywood for the bottom and sides and the top 
#  would be made of a cheap fake wood with similar properties to cardboard. 
#  I wanted to test the design with a cardboard top that would be light weight
#  and mobile while still being somewhat structurally sound. putting pressure 
#  on the switch had no issues on the integrity of the cardboard. It then came
#  down to testing the motors.
#
#  @section sec_testing Testing
#  Since I needed to test the motors before putting them into the box, so I 
#  tested all of the switches and the motors and the switch beforehand. They 
#  worked great! Turning the switch ON moved one motor, then the other, then 
#  reversed both back to their relative positions. Since that test went well, 
#  I installed the motors without the links they would be turning. Both were 
#  functioning perfect until I attached the arm for the door. The weight of 
#  the door causing a torque on the motor rotated the motor in a non-safe 
#  fashion ultimately causing the motor to not function with the set-up. The 
#  other motor still spins and after a few tests works perfect by rotating a 
#  pinion transmitting rotationto a spur gear with the lever arm attached 
#  ultimately turning the switch OFF.  To avoid further failures for now, I 
#  did not attach the functioning motor to the hatch opening links.
#
#  @section sec_conclusion Final Thoughts
#  I was really hoping this project would work by the due date without any 
#  issues. This class was convenient because I also took ME 329: Mechanical
#  Systems Design, I was able to calculate gear designs to create the gear 
#  train for this project. This was also a great opportunity to get familiar 
#  with using shop tools since I never had the opportunity to take a shop 
#  class in high school. Although I didn't finish the project I am impressed 
#  with the amount of information I learned from this quarter (Spring 2020) 
#  to be able to at least get the arm to function with precision and accuracy 
#  when turning the switch OFF. I am proud of this achievement because it 
#  shows that I have enough skill to program a microcontroller to perform
#  tasks according to the commands I give it in the code. I would say that 
#  with the pandemic, this summer is a good opportunity to hone these skills
#  along with improving my understanding mechanical system limitations to be 
#  able to build the robots I want to and become proficient in linking the 
#  software, mechanical, and electrical worlds.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date June 12, 2020