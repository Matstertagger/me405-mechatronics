## @Proposal.py
#  @author Matthew Tagupa, Trevor Blythe, and Jose Chavez
#  
#  @page page_Proposal ME 405 Term Project Proposal: "Do Nothing Box"
#
#  @section sec_title ME 405 Term Project Proposal: The Do Nothing Box
#
#  Team Members: Jose Chavez, Trevor Blythe, and Matthew Tagupa
#  Link: https://matstertagger.bitbucket.io
# 
#  @section sec_problem Problem Statement:
# 
#  Have you ever found yourself bored and doing nothing? We built a box that 
#  is just as bored and also does absolutely nothing to entertain itself. 
#  However, with great teamwork, both you and the “Do Nothing Box” can work 
#  together to entertain each other.
# 
#  Our “Do Nothing Box” has state of the art ME 405 features including a 
#  Nucleo board and motor driver to control two motors and a switch that will 
#  all be used for fun. The user will be tempted to turn on the switch 
#  activating the “Do Nothing Box”. At this point, the first motor in the box 
#  will open the box from the inside while the second motor controlling a 
#  mechanical arm in the box controlled by the motor will reach out of the box 
#  to turn the switch off then retract back into the box. The encoders built 
#  into the motors will be in charge of making sure that the correct amount of 
#  stress is placed on the switch and the lid of the box to function 
#  appropriately without damaging the motor or any of the parts inside of the 
#  motor. 
# 
#  @section sec_materials Bill of Materials:
# 
#  ●   A box, ambitiously wooden but cardboard will do
#  ●   Gears for the motor (need gears that can be put onto a shaft with 
#  dimensions: diameter = 3 mm) (some affordable options found on Amazon)
#  ●   Gears for the lever arms (Gear set on Amazon
#  ●   Shafts for the lever arm (Find gear set on Amazon)
#  ●   Wood for the lever arm (Home improvement store)
#  ●   Toggle Switch (Amazon)
# 
#  @section sec_plan Assembly Plan:
# 
#  Some of us have the tools for this project, for example the lever arm needs 
#  to be made possibly with wood which we can all get at a home improvement 
#  store, but not all of us have woodshop tools. We are devising a plan to 
#  make it easier for all members to be able to build. This includes, but is 
#  not limited to, building the arm with junk around the house like soda cans.
# 
# 
#  @section sec_safety Safety Assessment: 
# 
#  The project is mostly safe, with most of the danger coming from the use of 
#  hand tools during the construction and assembly. To stay safe, basic hand 
#  tool safety will be observed. There is a small amount of danger that may 
#  arise from sticking fingers in pinch points while the motor turns, but this 
#  will not be hard to avoid.
# 
#  @section sec_timeline General Timeline:
# 
#  We will start by ordering parts, so that we don’t have to wait for them to 
#  arrive. Testing the physical design will also start early, so that any 
#  potential problems can be ironed out while we still have time to purchase 
#  new parts. As this is taking place, we will also work on the code that 
#  controls the most basic functions. From here, we can iron out details, and 
#  then slowly add new features as time permits. We are planning on meeting 
#  every other day for an hour or two and for about 4 to 5 hours over the 
#  weekend to make sure that the code gets done and each member’s “Do Nothing 
#  Box” works perfect. We plan on having the code and a final product to test 
#  by the Tuesday of finals week (6/9). 