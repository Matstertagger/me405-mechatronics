## @file ToggleSwitch.py
#  The toggle switch is an external part that did not come with our kits. The 
#  switch is a simple ON/OFF switch that can be found on the following 
#  website:
#
#  SPST Mini Latching Toggle Switch: https://www.servocity.com/spst-mini-toggle-switch
#
#  The user will be responsible for designating the port that the switch is
#  connected to. This class is only meant to activate the switch and allow for
#  the user to designate what the switch will activate or deactivate. The user
#  can then call on the value (o or 1) by using the command 
#  "(name of pin).value()".
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date June 12, 2020
#
#  @package ToggleSwitch
#  The toggle switch is an external part that did not come with our kits. The 
#  switch is a simple ON/OFF switch that can be found on the following 
#  website:
#
#  The user will be responsible for designating the port that the switch is
#  connected to. This class is only meant to activate the switch and allow for
#  the user to designate what the switch will activate or deactivate. The user
#  can then call on the value (o or 1) by using the command 
#  "(name of pin).value()".
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date June 12, 2020

import pyb
import utime

## A toggle switch
#
#  This class initializes the port for the switch as an inout port
#
#  @author Matthew Tagupa
#  @copyright License Info
#  @date June 12, 2020
class ToggleSwitch:

    ## Constructor for the Toggle Switch
    #
    #  Creates a toggle switch by initializing GPIO pin designated by the user
    #  so that the microcontroller can read the values.
    #
    #  @param switch_pin The port location designated by the user
    def __init__ (self, switch_pin):

        #  These will store the variables input as class attributes
        self.switch_pin = switch_pin
        
        #  Initialize all of the pins
        self.Toggle_State = pyb.Pin(self.switch_pin, pyb.Pin.IN)
        
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.

    #  Create Input Pin Location
    Switch_Pin = pyb.Pin.cpu.A6
    
    #  Create object from class
    Toggle = ToggleSwitch(Switch_Pin)
    
    #  Loop and display the value of the switch input every second
    while (True):
        print('{:}'.format(Switch_Pin.value()))
        utime.sleep_ms(1000)