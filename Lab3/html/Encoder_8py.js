var Encoder_8py =
[
    [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ],
    [ "enco1", "Encoder_8py.html#a8d64a1499b1f602c167f6309c5850179", null ],
    [ "enco2", "Encoder_8py.html#aa0dd684fc6be6f31295b303cd9bc795a", null ],
    [ "EncoderA1", "Encoder_8py.html#a2d8419438ed5a81851aee9fae14cecfc", null ],
    [ "EncoderA2", "Encoder_8py.html#a52cd405ceb062611cffecaffbe872f50", null ],
    [ "EncoderB1", "Encoder_8py.html#aac389f2dbdcca9b604e0944b7c0bfffe", null ],
    [ "EncoderB2", "Encoder_8py.html#a72f3b8270029f9af1a5300b5c94f9cd1", null ],
    [ "period", "Encoder_8py.html#a05d60e7628c4dd162d798f42ecf3f8d5", null ],
    [ "prescaler", "Encoder_8py.html#ad54fd17cccea5d95c46b13c4d8f3a342", null ],
    [ "Timer1", "Encoder_8py.html#a665db189f3c96432d72d4ce624dd6ad0", null ],
    [ "Timer2", "Encoder_8py.html#a11d4ef1e3def1026fdcaa72c91428f6a", null ]
];